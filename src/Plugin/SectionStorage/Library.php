<?php

namespace Drupal\layout_library_st\Plugin\SectionStorage;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\layout_builder\Entity\LayoutBuilderSampleEntityGenerator;
use Drupal\layout_builder\Section;
use Drupal\layout_builder_st\TranslatableSectionStorageInterface;
use Drupal\layout_library\Plugin\SectionStorage\Library as OriginalLibrary;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Library overriden SectionStorage class.
 *
 * This class is based on the
 * \Drupal\layout_library\Plugin\SectionStorage\Library but implements the
 * \Drupal\layout_builder_st\TranslatableSectionStorageInterface.
 */
class Library extends OriginalLibrary implements TranslatableSectionStorageInterface {

  /**
   * The layout overrides array.
   *
   * @var array|NULL
   */
  protected $layoutOverrides = [];

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, LayoutBuilderSampleEntityGenerator $sampleEntityGenerator, protected LanguageManagerInterface $languageManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $sampleEntityGenerator);
  }

  /**
   * {@inheritDoc}
   */
  public function setTranslatedComponentConfiguration($uuid, array $configuration) {
    $this->initLayoutOverrides();

    $layout = $this->getLayout();
    $langcode = $this->getTranslationLanguage()->getId();

    foreach ($layout->getSections() as $sectionId => $section) {
      $components = $section->getComponents();

      $component = $components[$uuid] ?? FALSE;
      if (!$component) {
        continue;
      }

      $this->layoutOverrides[$langcode][$sectionId]['components'][$uuid]['configuration'] = $configuration;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getTranslatedComponentConfiguration($uuid) {
    $translatedConfig = $this->getTranslatedConfiguration();
    foreach ($translatedConfig as $section) {
      if (isset($section['components'][$uuid])) {
        return $section['components'][$uuid]['configuration'];
      }
    }
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getTranslatedConfiguration() {
    $this->initLayoutOverrides();

    $layout = $this->getLayout();
    $untranslatedData = $layout->getLayout();
    $untranslatedData = array_map(function(Section $section) {
      return $section->toArray();
    }, $untranslatedData);

    $langcode = $this->getTranslationLanguage()->getId();

    return NestedArray::mergeDeepArray([
      $untranslatedData,
      $this->layoutOverrides[$langcode],
    ], TRUE);
  }

  /**
   * {@inheritDoc}
   */
  public function isDefaultTranslation() {
    return $this->languageManager
      ->getCurrentLanguage(LanguageInterface::TYPE_URL)
      ->isDefault();
  }

  /**
   * {@inheritDoc}
   */
  public function getTranslationLanguage() {
    return $this->languageManager
      ->getCurrentLanguage(LanguageInterface::TYPE_URL);
  }

  /**
   * {@inheritDoc}
   */
  public function getSourceLanguage() {
    return $this->languageManager->getDefaultLanguage();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('layout_builder.sample_entity_generator'),
      $container->get('language_manager')
    );
  }

  /**
   * Initializes the $layoutOverrides property from the config override entity.
   */
  public function initLayoutOverrides(): void {
    $langcode = $this->getTranslationLanguage()->getId();

    if (isset($this->layoutOverrides[$langcode])) {
      return;
    }

    $layout = $this->getLayout();
    $configId = $layout->getConfigDependencyName();

    /** @var \Drupal\language\Config\LanguageConfigOverride $translatableConfig */
    $translatableConfig = $this->languageManager->getLanguageConfigOverride($langcode, $configId);
    $this->layoutOverrides[$langcode] = $translatableConfig->get()['layout'] ?? [];
  }

  /**
   * Returns the layout overrides.
   *
   * @param string|null $langcode
   *   The langcode of the overrides.
   *
   * @return array
   *   The layout overrides.
   */
  public function getLayoutOverrides(?string $langcode): array {
    $this->initLayoutOverrides();
    return $langcode ? $this->layoutOverrides[$langcode] ?? [] : $this->layoutOverrides;
  }

}
