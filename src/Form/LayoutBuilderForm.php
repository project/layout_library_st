<?php

namespace Drupal\layout_library_st\Form;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\layout_library\Form\LayoutBuilderForm as CoreLayoutBuilderForm;

/**
 * The overriden Layout Builder form.
 */
class LayoutBuilderForm extends CoreLayoutBuilderForm {

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $urlLanguage = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_URL);

    if ($urlLanguage->isDefault()) {
      $result = $this->sectionStorage->save();
    }
    else {
      $layoutConfiguration = $this->sectionStorage->getLayoutOverrides($urlLanguage->getId());
      $layout = $this->getEntity();

      $configId = $layout->getConfigDependencyName();
      $configOverride = \Drupal::languageManager()->getLanguageConfigOverride($urlLanguage->getId(), $configId);

      // The block is serialized the first time is added to the layout until
      // it's saved. The logic that unserializes the block_content and saves it
      // to the database is done in
      // \Drupal\layout_builder\Plugin\Block\InlineBlock::saveBlockContent that
      // is triggered in a entity_presave. However, we are not going to save the
      // Layout entity in order to add the translation, because we need to save
      // the config override (that's not an entity), so that logic won't be
      // triggered. That's the reason why we replicate that logic here.
      foreach($layoutConfiguration as $sectionId => &$components) {
        foreach($components['components'] as $uuid => &$componentData) {
          if (isset($componentData['configuration']['block_serialized'])) {
            $block = unserialize($componentData['configuration']['block_serialized']);

            if ($block instanceof BlockContent) {
              $block->save();
              unset($componentData['configuration']['block_serialized']);
            }
          }
        }
      }

      $configOverride->setData(['layout' => $layoutConfiguration]);
      $configOverride->save();
      $result = SAVED_UPDATED;
    }

    $this->layoutTempstoreRepository->delete($this->sectionStorage);
    $this->messenger()->addMessage($this->t('The layout has been saved.'));
    $form_state->setRedirectUrl($this->sectionStorage->getRedirectUrl());

    return $result;
  }

}
