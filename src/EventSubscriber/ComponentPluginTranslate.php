<?php

namespace Drupal\layout_library_st\EventSubscriber;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\LayoutEntityHelperTrait;
use Drupal\layout_builder_st\TranslationsHelperTrait;
use Drupal\layout_library_st\Plugin\SectionStorage\Library as SectionStorageLayoutLibrarySt;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class ComponentPluginTranslate implements EventSubscriberInterface {

  use LayoutEntityHelperTrait;
  //  This trait will be merged into core's.
  use TranslationsHelperTrait;

  /**
   * Creates a ComponentPluginTranslate object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   */
  public function __construct(private LanguageManagerInterface $languageManager, private RouteMatchInterface $routeMatch) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = ['onBuildRender', 200];
    return $events;
  }

  /**
   * Translates the plugin configuration if needed.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    if (!$this->languageManager->isMultilingual()) {
      return;
    }
    $plugin = $event->getPlugin();
    $contexts = $event->getContexts();
    $component = $event->getComponent();
    if (!$plugin instanceof ConfigurableInterface && !isset($contexts['layout_builder.entity'])) {
      return;
    }

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $contexts['layout_builder.entity']->getContextValue();
    if (!$entity instanceof TranslatableInterface) {
      return;
    }
    if ($entity->isDefaultTranslation()) {
      return;
    }
    if(!$entity->hasField('layout_builder__layout') || !$entity->hasField('layout_builder__translation') || empty($entity->get('layout_selection')->entity)) {
      return;
    }

    $configuration = $plugin->getConfiguration();

    $component_uuid = $component->getUuid();
    if (!$entity->get('layout_builder__layout')->isEmpty() && !empty($entity->get('layout_builder__translation')->value['components'][$component_uuid])) {
      $translated_plugin_configuration = $entity->get('layout_builder__translation')->value['components'][$component_uuid];
    }

    // If the block is not translated in the node, try to get the configuration
    // from the layout_library.
    if (empty($translated_plugin_configuration)) {
      $section_storage = $this->sectionStorageManager()->load('layout_library', $contexts);
      if (!$section_storage instanceof SectionStorageLayoutLibrarySt) {
        return;
      }
      if (!$entity->hasField('layout_selection') || $entity->get('layout_selection')->isEmpty()) {
        return;
      }
      if (!$section_storage->getContextValue('layout')) {
        $section_storage->setContextValue('layout', $entity->get('layout_selection')->entity);
      }
      $translated_plugin_configuration = $section_storage->getTranslatedComponentConfiguration($component_uuid);
    }

    if (!empty($translated_plugin_configuration)) {
      $translated_plugin_configuration += $configuration;
      $plugin->setConfiguration($translated_plugin_configuration);
    }
  }

}
